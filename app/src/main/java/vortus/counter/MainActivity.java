package vortus.counter;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    private final int SPINNER_COUNT = 3;
    private final int NUMBER_COUNT = 4;
    private Spinner spinners[] = new Spinner[SPINNER_COUNT]; // Spinner array
    private TextView numbers[] = new TextView[NUMBER_COUNT]; // Numbers

    protected void onCreate(Bundle savedInstanceState) { // Starting activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeNumbers(); // Initializing numbers
        initializeSpinners(); // Initializing spinners
    }

    // Calculating answer
    public void calculateResult(View view){ // Calculates the result

        for(int i = 0; i < NUMBER_COUNT; i++) { // If found empty numbers
            if (numbers[i].getText().toString().trim().length() == 0){
                Toast.makeText(this, "Yra tu\u0161\u010di\u0173 laukeli\u0173!", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        double result = Double.parseDouble(numbers[0].getText().toString()); // Setting first to be result

        TextView r = (TextView)findViewById(R.id.ResultView);
        for(int i = 1; i < NUMBER_COUNT; i++){
            result = calculateOperation(spinners[i - 1].getSelectedItem().toString(),
                    result, Double.parseDouble(numbers[i].getText().toString()));
        }
        r.setText(Double.toString(result));

    }

    private double calculateOperation(String operation, double a, double b){ // Simple calc funct
        if(operation.equals("+")) return a + b;
        else if(operation.equals("-")) return a - b;
        else if(operation.equals("*")) return a * b;
        return 0;
    }

    // Initializing stuff
    private  void initializeNumbers(){ // Initializing numbers
        for(int i = 0; i < NUMBER_COUNT - 1; i++)
            numbers[i] = (TextView) findViewById(R.id.Number1 + i * 2);
        numbers[NUMBER_COUNT - 1] = (TextView)findViewById(R.id.Number4);
    }

    private  void initializeSpinners(){ // Initialize spinners
        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.operators,
                android.R.layout.simple_spinner_item);

        for(int i = 0; i < SPINNER_COUNT; i++) { // Setting listener, adapter, spinner
            spinners[i] = (Spinner)findViewById(R.id.Spinner1 + i * 2);
            spinners[i].setAdapter(arrayAdapter);
            spinners[i].setOnItemSelectedListener(this);
        }

    }

    // Additional spinner stuff
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    public void onNothingSelected(AdapterView<?> parent) {

    }
}
